import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { User } from '../model/user';

export const usersStore = createSlice({
  name: 'users',
  initialState: [] as User[],
  reducers: {
    addUser(state, action: PayloadAction<Omit<User, 'isAdmin'>>) {
      const { id, name } = action.payload;
      state.push({ id, name, isAdmin: false })
    },
    deleteUser(state, action: PayloadAction<User>) {
      const index = state.findIndex(todo => todo.id === action.payload.id)
      state.splice(index, 1)
    },
    setAsAdmin(state, action: PayloadAction<User>) {
      const user = state.find(user => user.id === action.payload.id)
      if (user) {
        user.isAdmin = !user.isAdmin;
      }
    },
  }
});

export const { addUser, deleteUser, setAsAdmin } = usersStore.actions
// export default usersStore.reducer
